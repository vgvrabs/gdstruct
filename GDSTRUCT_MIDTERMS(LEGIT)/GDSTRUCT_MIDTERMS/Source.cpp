#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>
#include <conio.h>

using namespace std;

void main()
{
	srand(time(NULL));
	int userInput;
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	while (true)
	{
		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << " ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << " ";
		cout << endl << endl << "What do you want to do?"
			<< endl << "1: Remove element at an index"
			<< endl << "2: Search for an element"
			<< endl << "3: Expand and generate random values" << endl;
		cin >> userInput;

		switch (userInput)
		{
			int input;
		case 1:
		{
			cout << "Enter index to remove: ";
			cin >> input;
			cout << "Unordered Array: " << unordered[input]<< " at index " << input << " has been removed." << endl;
			unordered.remove(input);
			cout << "Ordered Array: " << ordered[input] << " at index " << input << " has been removed." << endl;
			ordered.remove(input);
			system("pause");
			break;
		}
		case 2:
		{
			cout << "\n\nEnter number to search: ";
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
			{
				cout << input << " was found at index " << result << ".\n";
			}
			else
				cout << input << " not found." << endl;

			
			cout << "Ordered Array(Binary Search):\n";
			int binaryResult = ordered.binarySearch(input);
			if (binaryResult >= 0)
			{
				cout << input << " was found at index " << binaryResult << ".\n";
			}
			else
				cout << input << " not found." << endl;
			system("pause");
			break;
		}
		case 3:
		{
			cout << "Input size of expansion: ";
			cin >> input;
			for (int i = 0; i < input; i++)
			{
				int randNumber = rand() % 101;
				unordered.push(randNumber);
				ordered.push(randNumber);
			}
			cout << "Arrays have been expanded!" << endl;
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " ";
			_getch();
			break;
		}
			
		}
		system("CLS");
	}
}