#include "UnorderedArray.h"
#include "Stack.h"
#include "Queue.h"
#include <iostream>
#include <string>
#include <conio.h>

using namespace std;



char action(char userInput) {

	cout << "What do you want to do? " << endl
		<< "[1] Push elements" << endl
		<< "[2] Pop elements" << endl
		<< "[3] Print everything then empty set" << endl;
		cin >> userInput;
	return userInput;
}

int main() {
	int size;
	char userInput = 'k';

	cout << "Input size for element sets: ";
	cin >> size;

	Stack<int> stack(size);
	Queue<int> queue(size);

	while (true) {


		system("CLS");
		userInput = action(userInput);

		switch (userInput) {

		case '1':

			int val;
			cout << "Enter number: ";
			cin >> val;
			stack.push(val);
			queue.push(val);
			cout << "Top of element sets: " << endl
				<< "Queue: " << queue.top() << endl
				<< "Stack: " << stack.top() << endl;
			system("pause");
			break;

		case '2':
			
			cout << "You have popped the top elements" << endl;
			_getch();
			
			queue.pop();
			stack.pop();

			if (stack.getSize() > 0 && queue.getSize() > 0) {
			
				cout << "Top of element sets: " << endl
					<< "Queue: " << queue.top() << endl
					<< "Stack: " << stack.top() << endl;
				system("pause");
			}
			break;

		case '3':

			cout << "Queue Elements: " << endl;
			for (int i = 0; i < queue.getSize(); i++) {

				cout << queue.displayIndex(i) << endl;
			}
			cout << "\nStack Elements: " << endl;
			for (int i = stack.getSize(); i > 0; --i) {

				cout << stack.displayIndex(i) << endl;
			}

			system("pause");
			queue.clear();
			stack.clear();
		}





	}


}