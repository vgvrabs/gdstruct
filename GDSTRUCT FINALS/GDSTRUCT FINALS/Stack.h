#pragma once
#include "UnorderedArray.h"
#include <iostream>
#include <string>

using namespace std;

template <class T>
class Stack {
public:

	Stack() {


	}

	Stack(int size){

		mArray = new UnorderedArray<T> (size);
	}

	virtual void push(T val) {

		mArray->push(val);
	}

	virtual int top() {

		return mArray[0][mArray->getSize() - 1];
	}


	UnorderedArray<T>* getArray() {

		return mArray;
	}

	int displayIndex(T val) {

		return mArray[0][val - 1];
	}

	void clear() {

		mArray->clear();
	}

	virtual void pop() {

		mArray->pop();
	}

	int getSize() {

		return mArray->getSize();
	}

	
private:
	UnorderedArray<T>* mArray;
};
