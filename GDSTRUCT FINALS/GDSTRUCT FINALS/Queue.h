#pragma once
#include "UnorderedArray.h"
#include <iostream>
#include <string>

using namespace std;

template <class T>
class Queue{
public:


	Queue (int size) {

		mArray = new UnorderedArray<T>(size);
	}

	virtual void push(T val) {

		mArray->push(val);
	}

	virtual int top() {

		return mArray[0][0];
	}


	UnorderedArray<T>* getArray() {

		return mArray;
	}

	int displayIndex(T val) {

		return mArray[0][val];
	}

	void clear() {

		mArray->clear();
	}

	virtual void pop() {

		mArray->remove(0);
	}

	int getSize() {

		return mArray->getSize();
	}



private:
	UnorderedArray<T>* mArray;
};
