#include <iostream>

using namespace std;

void printNumbersReverse(int from) {
	if (from <= 0)
		return;

	cout << from << " ";
	printNumbersReverse(from - 1);
}

int sumOfDigits(int input) {
	int sum;
	if (input == 0)
		return 0;
	
	return sum = ((input % 10) + sumOfDigits(input / 10)); // takes a single digit from the number   // recurs the function with one digit less																						
}

void printFibonacci(int input, int current, int previous) {
	input--;

	cout << current << " ";
	if (input == 0)
		return;

	int temp = current;
	current = current + previous;
	previous = temp;

	printFibonacci(input, current, previous);
}

bool isPrime(int input, int divisor) { // check if the number is prime
	if (input == 2)
		return (input == 2) ? true : false;
	else if (input % divisor == 0)
		return false;
	else if (divisor * divisor > input)
		return true;

	return isPrime(input, ++divisor);
}



int main() {
	int input;
	cout << "Input a number: ";
	cin >> input;
	cout << "the sum of the digits in " << input << " is : " << sumOfDigits(input) << endl;
	cout << "Input a number for Fibonacci: ";
	cin >> input;
	printFibonacci(input, 0, 1);
	cout << endl <<  "Input a number: ";
	cin >> input;
	if (isPrime(input, 2))
		cout << "Number is Prime";
	else
		cout << "Number is not Prime";

	return 0;
}